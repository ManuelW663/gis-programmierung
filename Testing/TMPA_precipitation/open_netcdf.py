from osgeo import gdal

netcdf_name = 'g4.timeAvg.TRMM_3B43_7_precipitation.19980101-20211231.MONTH_01.34E_3S_41E_3N.nc'

layer_name = "hs"

# Open netcdf file.nc with gdal
ds = gdal.Open("NETCDF:{0}".format(netcdf_name))

# Read full data from netcdf
data = ds.ReadAsArray(0, 0, ds.RasterXSize, ds.RasterYSize)
data[data < 0] = 0
